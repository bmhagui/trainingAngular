import {User} from '../models/User.models';
import {Subject} from 'rxjs';

export class UserService {
  private users: User[] = [
    {
      firstName: 'John',
      lastName: 'Doe',
      email: 'john@doe.com',
      drinkPreference: 'Coke',
      hobbies: ['Football', 'VideoGames']
    }
  ];
  userSubject = new Subject<User[]>();
  emitUsers() {
    this.userSubject.next(this.users.slice());
  }
  addUser(user: User) {
    this.users.push(user);
    this.emitUsers();
  }
}
